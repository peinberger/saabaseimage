FROM debian
FROM java:8
MAINTAINER Peter Einberger <peter.einberger@ipa.fraunhofer.de>

RUN java -version
RUN apt-get install -y maven